\chapter{Strings}
\label{stringschapter}

The \defschemelibrary{strings} library contains the essential
procedures that operate on strings, as well as a few extra utilities.

Strings are sequences of characters.

\vest The {\em length} of a string is the number of characters that it
contains.  This number is fixed when the
string is created.  The \defining{valid indices} of a string are the
integers less than the length of the string.  The first
character of a string has index 0, the second has index 1, and so on.

\begin{entry}{%
\proto{string?}{ obj}{procedure}}

Returns \schtrue{} if \var{obj} is a string, otherwise returns \schfalse.
\end{entry}


\begin{entry}{%
\proto{make-string}{ k}{procedure}
\rproto{make-string}{ k char}{procedure}}

Returns a newly allocated string of
length \var{k}.  If \var{char} is given, then all elements of the string
are initialized to \var{char}, otherwise the contents of the
\var{string} are unspecified.

\end{entry}

\begin{entry}{%
\proto{string}{ char \dotsfoo}{procedure}}

Returns a newly allocated string composed of the arguments.

\end{entry}

\begin{entry}{%
\proto{string-length}{ string}{procedure}}

Returns the number of characters in the given \var{string} as an exact
integer object.
\end{entry}


\begin{entry}{%
\proto{string-ref}{ string k}{procedure}}

\domain{\var{K} must be a valid index of \var{string}.}
The {\cf string-ref} procedure returns character \vr{k} of \var{string} using zero-origin indexing.

\begin{note}
  Implementors should make {\cf string-ref} run in constant
  time.
\end{note}
\end{entry}

\begin{entry}{%
\proto{string=?}{ \vari{string} \varii{string} \variii{string} \dotsfoo}{procedure}}

Returns \schtrue{} if the strings are the same length and contain the same
characters in the same positions.  Otherwise, the {\cf string=?}
procedure returns \schfalse.

\begin{scheme}
(string=? "Stra\ss{}e" "Strasse") \lev \schfalse%
\end{scheme}
\end{entry}

\begin{entry}{%
\proto{string<?}{ \vari{string} \varii{string} \variii{string} \dotsfoo}{procedure}
\proto{string>?}{ \vari{string} \varii{string} \variii{string} \dotsfoo}{procedure}
\proto{string<=?}{ \vari{string} \varii{string} \variii{string} \dotsfoo}{procedure}
\proto{string>=?}{ \vari{string} \varii{string} \variii{string} \dotsfoo}{procedure}}

These procedures are the lexicographic extensions to strings of the
corresponding orderings on characters.  For example, {\cf string<?}\ is
the lexicographic ordering on strings induced by the ordering
{\cf char<?}\ on characters.  If two strings differ in length but
are the same up to the length of the shorter string, the shorter string
is considered to be lexicographically less than the longer string.

\begin{scheme}
(string<? "z" "\ss") \ev \schtrue
(string<? "z" "zz") \ev \schtrue
(string<? "z" "Z") \ev \schfalse%
\end{scheme}
\end{entry}


\begin{entry}{%
\proto{substring}{ string start end}{procedure}}

\domain{\var{String} must be a string, and \var{start} and \var{end}
must be exact integer objects satisfying
$$0 \leq \var{start} \leq \var{end} \leq \hbox{\tt(string-length \var{string})\rm.}$$}
The {\cf substring} procedure returns a newly allocated string formed from the characters of
\var{string} beginning with index \var{start} (inclusive) and ending with index
\var{end} (exclusive).
\end{entry}


\begin{entry}{%
\proto{string-append}{ \var{string} \dotsfoo}{procedure}}

Returns a newly allocated string whose characters form the concatenation of the
given strings.
\end{entry}


\begin{entry}{%
\proto{string->list}{ string}{procedure}
\proto{list->string}{ list}{procedure}}

\domain{\var{List} must be a list of characters.}
The {\cf string\coerce{}list} procedure returns a newly allocated list of the
characters that make up the given string.  The {\cf
  list\coerce{}string} procedure
returns a newly allocated string formed from the characters in
\var{list}. The {\cf string\coerce{}list}
and {\cf list\coerce{}string} procedures are
inverses so far as {\cf equal?}\ is concerned.
\end{entry}

\begin{entry}{%
\proto{string-for-each}{ proc \vari{string} \varii{string} \dotsfoo}{procedure}}

\domain{The \var{string}s must all have the same length.  \var{Proc}
  should accept as many arguments as there are {\it string}s.}
The {\cf string-for-each} procedure applies \var{proc}
element-wise to the characters of the
\var{string}s for its side effects,  in order from the first characters to the
last.
\var{Proc} is always called in the same dynamic environment
as {\cf string-for-each} itself.
The return values of {\cf string-for-each} \areunspecified.

Analogous to {\cf for-each}.

\implresp The implementation must check the restrictions
on \var{proc} to the extent performed by applying it as described.
An
implementation may check whether \var{proc} is an appropriate argument
before applying it.
\end{entry}

\begin{entry}{%
\proto{string-copy}{ string}{procedure}}

Returns a newly allocated copy of the given \var{string}.

\end{entry}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "r6rs-lib"
%%% End:
