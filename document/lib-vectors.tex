\chapter{Vectors}
\label{vectorschapter}

The \defschemelibrary{vectors} library contains the essential
procedures that operate on vectors, as well as a few extra utilities.

Vectors are heterogeneous structures whose elements are indexed
by integers.  A vector typically occupies less space than a list
of the same length, and the average time needed to access a randomly
chosen element is typically less for the vector than for the list.

\vest The {\em length} of a vector is the number of elements that it
contains.  This number is a non-negative integer that is fixed when the
vector is created.
The {\em valid indices}\index{valid indices} of a
vector are the exact non-negative integer objects less than the length of the
vector.  The first element in a vector is indexed by zero, and the last
element is indexed by one less than the length of the vector.

Like list constants, vector constants must be quoted:

\begin{scheme}
'\#(0 (2 2 2 2) "Anna")  \lev  \#(0 (2 2 2 2) "Anna")%
\end{scheme}

\begin{entry}{%
\proto{vector?}{ obj}{procedure}}

Returns \schtrue{} if \var{obj} is a vector.  Otherwise the procedure
returns \schfalse.
\end{entry}


\begin{entry}{%
\proto{make-vector}{ k}{procedure}
\rproto{make-vector}{ k fill}{procedure}}

Returns a newly allocated vector of \var{k} elements.  If a second
argument is given, then each element is initialized to \var{fill}.
Otherwise the initial contents of each element is unspecified.

\end{entry}


\begin{entry}{%
\proto{vector}{ obj \dotsfoo}{procedure}}

Returns a newly allocated vector whose elements contain the given
arguments.  Analogous to {\cf list}.

\begin{scheme}
(vector 'a 'b 'c)               \ev  \#(a b c)%
\end{scheme}
\end{entry}


\begin{entry}{%
\proto{vector-length}{ vector}{procedure}}

Returns the number of elements in \var{vector} as an exact integer object.
\end{entry}


\begin{entry}{%
\proto{vector-ref}{ vector k}{procedure}}

\domain{\var{K} must be a valid index of \var{vector}.}
The {\cf vector-ref} procedure returns the contents of element \vr{k} of
\var{vector}.

\begin{scheme}
(vector-ref '\#(1 1 2 3 5 8 13 21) 5)  \lev  8%
\end{scheme}
\end{entry}


\begin{entry}{%
\proto{vector-set!}{ vector k obj}{procedure}}

\domain{\var{K} must be a valid index of \var{vector}.}
The {\cf vector-set!} procedure stores \var{obj} in element \vr{k} of
\var{vector}, and returns \unspecifiedreturn.

Passing an immutable vector to {\cf vector-set!} should cause an exception
with condition type {\cf\&assertion} to be raised.

\begin{scheme}
(let ((vec (vector 0 '(2 2 2 2) "Anna")))
  (vector-set! vec 1 '("Sue" "Sue"))
  vec)      \lev  \#(0 ("Sue" "Sue") "Anna")

(vector-set! '\#(0 1 2) 1 "doe")  \lev  \unspecified
             ; \textrm{constant vector}
             ; \textrm{should raise} \exception{\&assertion}%
\end{scheme}

\end{entry}


\begin{entry}{%
\proto{vector->list}{ vector}{procedure}
\proto{list->vector}{ list}{procedure}}

The {\cf vector->list} procedure returns a newly allocated list of the objects contained
in the elements of \var{vector}.  The {\cf list->vector} procedure returns a newly
created vector initialized to the elements of the list \var{list}.

\begin{scheme}
(vector->list '\#(dah dah didah))  \lev  (dah dah didah)
(list->vector '(dididit dah))   \lev  \#(dididit dah)%
\end{scheme}
\end{entry}


\begin{entry}{%
\proto{vector-fill!}{ vector fill}{procedure}}

Stores \var{fill} in every element of \var{vector}
and returns \unspecifiedreturn.
\end{entry}

\begin{entry}{%
\proto{vector-map}{ proc \vari{vector} \varii{vector} \dotsfoo}{procedure}}

\domain{The \var{vector}s must all have the same length.  \var{Proc}
  should accept as many arguments as there are {\it vector}s and return a
  single value.}

The {\cf vector-map} procedure applies \var{proc} element-wise to the elements of the
\var{vector}s and returns a vector of the results, in order.
\var{Proc} is always called in the same dynamic environment
as {\cf vector-map} itself.
The order in which \var{proc} is applied to the elements of the
\var{vector}s is unspecified.
If multiple returns occur from {\cf vector-map}, the return
values returned by earlier returns are not mutated.


Analogous to {\cf map}.

\implresp The implementation must check the restrictions
on \var{proc} to the extent performed by applying it as described.
An
implementation may check whether \var{proc} is an appropriate argument
before applying it.
\end{entry}


\begin{entry}{%
\proto{vector-for-each}{ proc \vari{vector} \varii{vector} \dotsfoo}{procedure}}

\domain{The \var{vector}s must all have the same length.  \var{Proc}
  should accept as many arguments as there are {\it vector}s.}
The {\cf vector-for-each} procedure applies \var{proc}
element-wise to the elements of the
\var{vector}s for its side effects,  in order from the first elements to the
last.
\var{Proc} is always called in the same dynamic environment
as {\cf vector-for-each} itself.
The return values of {\cf vector-for-each} \areunspecified.

Analogous to {\cf for-each}.

\implresp The implementation must check the restrictions
on \var{proc} to the extent performed by applying it as described.
An
implementation may check whether \var{proc} is an appropriate argument
before applying it.
\end{entry}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "r6rs-lib"
%%% End:
